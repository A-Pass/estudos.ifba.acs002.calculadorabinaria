from curses.ascii import isdigit

def isDecimal(value):
    """
    Modulo para determinar se um valor é decimal
    """
    value = str(value)
    return value and value.isdigit()

def isBinary(value):
    value = str(value)
    for c in value:
        if c not in '01':
            return False
    return True and value

def isOctal(value):
    value = str(value)
    for c in value:
        if c not in '01234567':
            return False
    return True and value

def isHexadecimal(value):
    value = str(value)
    for c in value:
        if c not in '0123456789ABCDEF':
            return False
    return True and value


def __trimZeroLeft(value):
    """
    Função que limpa os zeros à esquerda
    """
    # Passa por todos os caracteres, iniciando pelo primeiro
    # se for '0' ele remove primeiro e continua sucessivamente até encontrar um 
    # caractere diferente de '0'
    for c in value:
        if c == '0' and value != '0':
            value = value[1:]
        else:
            return value
    return value

def __binaryValidation(binaryValue):
    """
    Módulo que valida a entrada para binário e lança um erro se não for válida
    """
    if not isBinary:
        raise ValueError('Binário inválido')

def decimalToBinary(decimalValue):
    """
    Módulo que faz a conversão de decimal para binário
    """
    decimalValue = int(decimalValue)
    if  decimalValue == 0:
        return '0'

    restConcat = ''
    # faz várioas divisões por 2, até chegar em 1
    while decimalValue > 0:
        restConcat += str(decimalValue % 2)
        # faz divisao inteira, quando decimal = 1, atribui 0 e para a repetição
        decimalValue //= 2
    # inverte a string utilizando as funções de colchetes de string, [::-1] pega cada caractere na ordem invertida
    binaryValue = restConcat[::-1]

    return __trimZeroLeft(binaryValue)

def binaryToDecimal(binaryValue):
    __binaryValidation(binaryValue)

    binaryValue = str(binaryValue)
    pot = len(binaryValue)-1
    decimalValue = 0
    i = 0 
    while pot > -1:
        bit = binaryValue[i]
        
        if bit != '0':
            decimalValue += 2 ** pot
        
        i += 1
        pot -= 1
    return decimalValue

def octalToBinary(octalValue):
    octalValue = str(octalValue)
    binaryValue = ''
    for c in octalValue:
        if c not in '01234567':
            raise ValueError('Octal inválido')
        binaryValue += ('000' + decimalToBinary(int(c)))[-3:]
    return __trimZeroLeft(binaryValue)


def binaryToOctal(binaryValue):
    binaryValue = str(binaryValue)
    __binaryValidation(binaryValue)

    octalValue = ''

    binaryValue = '000' + binaryValue
    for i in range(len(binaryValue), 3, -3):
        octalValue = str(binaryToDecimal(binaryValue[i-3:i])) + octalValue
    
    return octalValue

def hexadecimalToBinary(hexValue):
    hexValue = str(hexValue)
    binaryValue = ''
    for c in hexValue:
        c_ = None
        if c.isdigit():
            c_ = c
        else: 
            match c: 
                case 'A':
                    c_ = 10      
                case 'B':
                    c_ = 11
                case 'C':
                    c_ = 12
                case 'D':
                    c_ = 13
                case 'E':
                    c_ = 14
                case 'F':
                    c_ = 15
                case _:
                    raise ValueError('Hexadecimal Inválido!')
        binaryValue += ('0000' + decimalToBinary(c_))[-4:]
    return __trimZeroLeft(binaryValue)

def binaryToHexadecimal(binaryValue):
    binaryValue = str(binaryValue)

    __binaryValidation(binaryValue)

    hexadecimalValue = ''

    binaryValue = '0000' + binaryValue
    for i in range(len(binaryValue), 4, -4):
        h = str(binaryToDecimal(binaryValue[i-4:i]))
        match h:
            case '10':
                h = 'A'      
            case '11':
                h = 'B'
            case '12':
                h = 'C'
            case '13':
                h = 'D'
            case '14':
                h = 'E'
            case '15':
                h = 'F'
        hexadecimalValue = h  + hexadecimalValue

    return __trimZeroLeft(hexadecimalValue)
        
    

    
