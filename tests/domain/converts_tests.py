from domain import converters
from tests import assertThat

def when_call_decimal_to_binary_converter():
    
    assertThat('0', converters.decimalToBinary(0)) 
    assertThat('1', converters.decimalToBinary(1))
    assertThat('101', converters.decimalToBinary(5))

    print('when_call_decimal_to_binary_converter: true')

def when_call_binary_to_decimal_converter():
    assertThat(0, converters.binaryToDecimal('0')) 
    assertThat(1, converters.binaryToDecimal('1'))
    assertThat(5, converters.binaryToDecimal('101'))
    assertThat(5, converters.binaryToDecimal('0101'))
    print('when_call_binary_to_decimal_converter: true')

def when_call_binary_to_octal_converter():
    assertThat('0', converters.binaryToOctal('0')) 
    assertThat('1', converters.binaryToOctal('1'))
    assertThat('5', converters.binaryToOctal('101'))
    assertThat('7', converters.binaryToOctal('111'))
    assertThat('37', converters.binaryToOctal('11111'))
    assertThat('37', converters.binaryToOctal('011111'))
    print('when_call_binary_to_octal_converter: true')

def when_call_octal_to_binary_converter():
    assertThat('0', converters.octalToBinary('0')) 
    assertThat('1', converters.octalToBinary('1'))
    assertThat('101', converters.octalToBinary('5'))
    assertThat('111', converters.octalToBinary('7'))
    assertThat('11111', converters.octalToBinary('37'))
    assertThat('11111', converters.octalToBinary('037'))
    print('when_call_octal_to_binary_converter: true')

def when_call_hexadecimal_to_binary_converter():
    assertThat('0', converters.hexadecimalToBinary('0'))
    assertThat('1', converters.hexadecimalToBinary('1'))
    assertThat('1010', converters.hexadecimalToBinary('A'))
    assertThat('1111', converters.hexadecimalToBinary('F'))
    assertThat('11111111', converters.hexadecimalToBinary('00FF'))
    assertThat('1001', converters.hexadecimalToBinary('9'))

    print('when_call_hexadecimal_to_binary_converter: true')

def when_call_binary_to_hexadecimal_converter():

    assertThat('0', converters.binaryToHexadecimal('00'))
    assertThat('1', converters.binaryToHexadecimal('1'))
    assertThat('F', converters.binaryToHexadecimal('1111'))
    assertThat('A', converters.binaryToHexadecimal('1010'))
    assertThat('9', converters.binaryToHexadecimal('1001'))
    assertThat('F', converters.binaryToHexadecimal('00001111'))

    print('when_call_binary_to_hexadecimal_converter: true')

def when_call_is_binary_check():
    assertThat(False, converters.isBinary('02'))

    print('when_call_is_binary_check: true')