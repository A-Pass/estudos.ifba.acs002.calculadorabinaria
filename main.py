from tkinter import *
from localization import strings
from domain import converters

# Variaveis globais para manipulação da interface

APP_MAIN_UI = None # type: Tk

RESULT_LABEL_PRIMARY = None # type: StringVar
RESULT_ENTRY_PRIMARY = None # Type: Entry
RESULT_ENTRY_PRIMARY_VALUE = None # type: StringVar
PRIMARY_IS_VALID_FOR_CONVERSION = None # function, armazena a função que valida a conversão
PRIMARY_CALLBACK_CONVERTER = None # function, armazena a função para conversão

RESULT_LABEL_SECONDARY = None # type: StringVar
RESULT_ENTRY_SECONDARY = None # Type: Entry
RESULT_ENTRY_SECONDARY_VALUE = None # type: StringVar

SECONDARY_IS_VALID_FOR_CONVERSION = None # function, armazena a função que valida a conversão
SECONDARY_CALLBACK_CONVERTER = None # function, armazena a função para conversão

def setupUI():
    """
    Modulo de inicialização da interface
    """
    setupMainUI()
    setupFeaturesOptionsUI()

def setupMainUI():
    """
    Modulo de inicialização da janela principal (root)
    """
    # Utiliza o escopo global para a variável
    global APP_MAIN_UI
    
    # Cria a janela principal e adiciona na variável global
    APP_MAIN_UI = Tk()
    APP_MAIN_UI.title('Calculadora')
    APP_MAIN_UI.geometry('400x300+400+300')
    APP_MAIN_UI.minsize(400, 300)
    APP_MAIN_UI.maxsize(400, 300)
    
    # Título que aparece na tela principal
    title = Label(APP_MAIN_UI, text=strings.APP_NAME)
    title.grid(row=0)

    # Inicializa a parte de resultados (entradas)
    setupResultSectionUI()

    # Inicializa a parte opções (opções de conversão)
    setupFeaturesOptionsUI()

    # Inicializa o sistema com a conversão decimal
    selectDecBin()


def setupResultSectionUI():
    """
    Módulo de inicialização da seção de resultados (compos de entradas)
    """

    # Inicializa o primeiro campo de entrada (conjunto entry/label)
    setupResultPrimary()

    # Inicializa o segundo campo de entrada (conjunto entry/label)
    setupResultSecondary()
   
def setupResultPrimary():
    """
    Módulo de inicialização da primeira seção de resultados
    """
    global APP_MAIN_UI, RESULT_LABEL_PRIMARY, RESULT_ENTRY_PRIMARY_VALUE, RESULT_ENTRY_PRIMARY
    
    # Cria um StringVar objeto e adiciona na váriavel global para acesso posterior

    RESULT_ENTRY_PRIMARY_VALUE = StringVar()
    # trace_add permite adcionar uma função ao evento de escrito do Stringvar,
    # ou seja, toda vez que o valor é modificado, vai disparar a função do callback
    RESULT_ENTRY_PRIMARY_VALUE.trace_add('write', callback = primaryEntryCallback)
   
    # Cria o rótulo do campo primário e adiciona na variável global 

    RESULT_LABEL_PRIMARY = StringVar()
    label = Label(APP_MAIN_UI, textvariable = RESULT_LABEL_PRIMARY)
    # .grid define o layout como um grade, row determina em qual linha vai aparecer e column determina a coluna na grade, ambos iniciando em 0
    label.grid(row=1, column=0)

    # Cria um campo e coloca na variável global
    RESULT_ENTRY_PRIMARY = Entry(APP_MAIN_UI, textvariable = RESULT_ENTRY_PRIMARY_VALUE)
    RESULT_ENTRY_PRIMARY.grid(row=2, column=0)

def setupResultSecondary():
    """
    Módulo de inicialização do campo de resultado secundário
    """
    global APP_MAIN_UI, RESULT_LABEL_SECONDARY, RESULT_ENTRY_SECONDARY_VALUE, RESULT_ENTRY_SECONDARY
    
    RESULT_ENTRY_SECONDARY_VALUE = StringVar()
    RESULT_ENTRY_SECONDARY_VALUE.trace_add('write', callback= secondaryEntryCallback)
    
    RESULT_LABEL_SECONDARY = StringVar()
    label = Label(APP_MAIN_UI, textvariable = RESULT_LABEL_SECONDARY)
    label.grid(row=1, column=1)

    RESULT_ENTRY_SECONDARY = Entry(APP_MAIN_UI, textvariable = RESULT_ENTRY_SECONDARY_VALUE)
    RESULT_ENTRY_SECONDARY.grid(row=2, column=1, columnspan=3)

def restoreDefaultConfig(entry):
    """
    Módulo para restaurar a cor dos campos de entradas para o padrão
    """
    entry.configure(highlightbackground='black', highlightcolor='black')

def primaryEntryCallback(var, index, mode):
    """
    Módulo de callback para o campo primário, disparado quando o usuário escreve no campo
    """
    global APP_MAIN_UI
    global RESULT_ENTRY_PRIMARY
    global RESULT_ENTRY_PRIMARY_VALUE, RESULT_ENTRY_SECONDARY_VALUE
    global PRIMARY_CALLBACK_CONVERTER, PRIMARY_IS_VALID_FOR_CONVERSION
    
    value = RESULT_ENTRY_PRIMARY_VALUE.get().strip()
    target = RESULT_ENTRY_SECONDARY_VALUE

    # Verifica se quando o evento foi disparado o campo relacionado era o que estava focado
    # necessário para evitar de limpar o campo errado quando houver erro
    if APP_MAIN_UI.focus_get() == RESULT_ENTRY_PRIMARY:
        # verifica se o valor inserido é válido para conversão
        if PRIMARY_IS_VALID_FOR_CONVERSION(value):
            restoreDefaultConfig(RESULT_ENTRY_PRIMARY)
            converted = PRIMARY_CALLBACK_CONVERTER(value)

            if converted != target.get().strip():
                target.set(converted)
        elif value:
            RESULT_ENTRY_PRIMARY.configure(highlightbackground='red', highlightcolor='red')
            target.set('')
        else:
            restoreDefaultConfig(RESULT_ENTRY_PRIMARY)
            target.set('')

def secondaryEntryCallback(var, index, mode):
    global RESULT_ENTRY_SECONDARY
    global RESULT_ENTRY_PRIMARY_VALUE, RESULT_ENTRY_SECONDARY_VALUE
    global SECONDARY_CALLBACK_CONVERTER, SECONDARY_IS_VALID_FOR_CONVERSION
    
    value = RESULT_ENTRY_SECONDARY_VALUE.get().strip()
    target = RESULT_ENTRY_PRIMARY_VALUE

    if APP_MAIN_UI.focus_get() == RESULT_ENTRY_SECONDARY:
        if SECONDARY_IS_VALID_FOR_CONVERSION(value):
            restoreDefaultConfig(RESULT_ENTRY_SECONDARY)
            converted = SECONDARY_CALLBACK_CONVERTER(value)

            if converted != target.get().strip():
                target.set(converted)
        elif value:
            # Modifica a borda do campo para vermelho, caso de erro
            RESULT_ENTRY_SECONDARY.configure(highlightbackground='red', highlightcolor='red')
            target.set('')
        else:
            restoreDefaultConfig(RESULT_ENTRY_SECONDARY)
            target.set('')


def setupFeaturesOptionsUI():
    setupDecBinButton()
    setupOcBinButton()
    setupHexBinButton()
    setupNumPad() 

def setupDecBinButton():
    global APP_MAIN_UI
    bt = Button(APP_MAIN_UI, text = strings.CONVERT_DECIMAL_BINARY, command=selectDecBin)
    # strick define que o componente vai preencher todo o espaço disponível da grid atribuida a ele
    bt.grid(row=4, sticky='nsew')

def setupOcBinButton():
    global APP_MAIN_UI
    bt = Button(APP_MAIN_UI, text = strings.CONVERT_OCTAL_BINARY, command=selectOcBin)
    bt.grid(row=5, sticky='nsew')

def setupHexBinButton():
    global APP_MAIN_UI
    bt = Button(APP_MAIN_UI, text = strings.CONVERT_HEXADECIMAL_BINARY, command=selectHexBin)
    bt.grid(row=6, sticky='nsew')

def numPadAdd(value):
    global APP_MAIN_UI

    value = str(value)
    # pega o elementro que está focado
    focus = APP_MAIN_UI.focus_get()
    focus.insert(INSERT, value)

def numPadClean():
    global RESULT_ENTRY_PRIMARY_VALUE, RESULT_ENTRY_SECONDARY_VALUE
    
    RESULT_ENTRY_PRIMARY_VALUE.set('')
    RESULT_ENTRY_SECONDARY_VALUE.set('')

def numPadErase():
    global APP_MAIN_UI
    global RESULT_ENTRY_PRIMARY_VALUE, RESULT_ENTRY_SECONDARY_VALUE
    global RESULT_ENTRY_PRIMARY, RESULT_ENTRY_SECONDARY

    focus = APP_MAIN_UI.focus_get()
    if focus == RESULT_ENTRY_PRIMARY:
        RESULT_ENTRY_PRIMARY_VALUE.set(RESULT_ENTRY_PRIMARY_VALUE.get()[:-1])
    elif focus == RESULT_ENTRY_SECONDARY:
        RESULT_ENTRY_SECONDARY_VALUE.set(RESULT_ENTRY_SECONDARY_VALUE.get()[:-1])

def setupNumPad():
    global APP_MAIN_UI

    nRow = 8

    erase = Button(APP_MAIN_UI, text = strings.ERASE, command=numPadErase)
    erase.grid(row = nRow, column = 3, sticky='nsew')

    clean = Button(APP_MAIN_UI, text = strings.CLEAN, command=numPadClean)
    clean.grid(row = nRow, column = 2, sticky='nsew')

    zero = Button(APP_MAIN_UI, text = strings.ZERO, command=lambda: numPadAdd(strings.ZERO))
    zero.grid(row = nRow, column = 1, sticky='nsew')

    nRow -= 1

    one = Button(APP_MAIN_UI, text = strings.ONE, command=lambda: numPadAdd(strings.ONE))
    one.grid(row = nRow, column = 1, sticky='nsew')
    
    two = Button(APP_MAIN_UI, text = strings.TWO, command=lambda: numPadAdd(strings.TWO))
    two.grid(row = nRow, column = 2, sticky='nsew')

    three = Button(APP_MAIN_UI, text = strings.THREE, command=lambda: numPadAdd(strings.THREE))
    three.grid(row = nRow, column = 3, sticky='nsew')

    nRow -= 1

    four = Button(APP_MAIN_UI, text = strings.FOUR, command=lambda: numPadAdd(strings.FOUR))
    four.grid(row = nRow, column = 1, sticky='nsew')

    five = Button(APP_MAIN_UI, text = strings.FIVE, command=lambda: numPadAdd(strings.FIVE))
    five.grid(row = nRow, column = 2, sticky='nsew')

    six = Button(APP_MAIN_UI, text = strings.SIX, command=lambda: numPadAdd(strings.SIX))
    six.grid(row = nRow, column = 3, sticky='nsew')

    nRow -= 1

    seven = Button(APP_MAIN_UI, text = strings.SEVEN, command=lambda: numPadAdd(strings.SEVEN))
    seven.grid(row = nRow, column = 1, sticky='nsew')
    
    eight = Button(APP_MAIN_UI, text = strings.EIGHT, command=lambda: numPadAdd(strings.EIGHT))
    eight.grid(row = nRow, column = 2, sticky='nsew')
    
    nine = Button(APP_MAIN_UI, text = strings.NINE, command=lambda: numPadAdd(strings.NINE))
    nine.grid(row = nRow, column = 3, sticky='nsew')

    nRow -= 1

    hex_a = Button(APP_MAIN_UI, text = strings.HEX_A, command=lambda: numPadAdd(strings.HEX_A))
    hex_a.grid(row = nRow, column=1, sticky='nsew')

    hex_b = Button(APP_MAIN_UI, text = strings.HEX_B, command=lambda: numPadAdd(strings.HEX_A))
    hex_b.grid(row = nRow, column=2, sticky='nsew')

    hex_c = Button(APP_MAIN_UI, text = strings.HEX_C, command=lambda: numPadAdd(strings.HEX_C))
    hex_c.grid(row = nRow, column=3, sticky='nsew')

    nRow -= 1

    hex_d = Button(APP_MAIN_UI, text = strings.HEX_D, command=lambda: numPadAdd(strings.HEX_D))
    hex_d.grid(row = nRow, column=1, sticky='nsew')

    hex_e = Button(APP_MAIN_UI, text = strings.HEX_E, command=lambda: numPadAdd(strings.HEX_E))
    hex_e.grid(row = nRow, column=2, sticky='nsew')

    hex_f = Button(APP_MAIN_UI, text = strings.HEX_F, command=lambda: numPadAdd(strings.HEX_F))
    hex_f.grid(row = nRow, column=3, sticky='nsew')

def setupBinaryPlusUI():
    global APP_MAIN_UI
    entry1 = tk.Entry(APP_MAIN_UI)
    entry2 = tk.Entry(APP_MAIN_UI)
    entry1.grid()
    entry2.grid()

def selectDecBin():
    global RESULT_LABEL_PRIMARY, RESULT_LABEL_SECONDARY
    global PRIMARY_CALLBACK_CONVERTER, SECONDARY_CALLBACK_CONVERTER
    global PRIMARY_IS_VALID_FOR_CONVERSION, SECONDARY_IS_VALID_FOR_CONVERSION

    RESULT_LABEL_PRIMARY.set(strings.DECIMAL)
    PRIMARY_IS_VALID_FOR_CONVERSION = converters.isDecimal
    PRIMARY_CALLBACK_CONVERTER = converters.decimalToBinary

    RESULT_LABEL_SECONDARY.set(strings.BINARY)
    SECONDARY_IS_VALID_FOR_CONVERSION = converters.isBinary
    SECONDARY_CALLBACK_CONVERTER = converters.binaryToDecimal
    numPadClean()

def selectHexBin():
    global RESULT_LABEL_PRIMARY, RESULT_LABEL_SECONDARY
    global PRIMARY_CALLBACK_CONVERTER, SECONDARY_CALLBACK_CONVERTER
    global PRIMARY_IS_VALID_FOR_CONVERSION, SECONDARY_IS_VALID_FOR_CONVERSION

    RESULT_LABEL_PRIMARY.set(strings.HEXADECIMAL)
    PRIMARY_IS_VALID_FOR_CONVERSION = converters.isHexadecimal
    PRIMARY_CALLBACK_CONVERTER = converters.hexadecimalToBinary

    RESULT_LABEL_SECONDARY.set(strings.BINARY)
    SECONDARY_IS_VALID_FOR_CONVERSION = converters.isBinary
    SECONDARY_CALLBACK_CONVERTER = converters.binaryToHexadecimal
    numPadClean()

def selectOcBin():
    global RESULT_LABEL_PRIMARY, RESULT_LABEL_SECONDARY
    global PRIMARY_CALLBACK_CONVERTER, SECONDARY_CALLBACK_CONVERTER
    global PRIMARY_IS_VALID_FOR_CONVERSION, SECONDARY_IS_VALID_FOR_CONVERSION

    RESULT_LABEL_PRIMARY.set(strings.OCTAL)
    PRIMARY_IS_VALID_FOR_CONVERSION = converters.isOctal
    PRIMARY_CALLBACK_CONVERTER = converters.octalToBinary

    RESULT_LABEL_SECONDARY.set(strings.BINARY)
    SECONDARY_IS_VALID_FOR_CONVERSION = converters.isBinary
    SECONDARY_CALLBACK_CONVERTER = converters.binaryToOctal
    numPadClean()
setupUI()
APP_MAIN_UI.mainloop()
